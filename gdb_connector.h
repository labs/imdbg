#pragma once

#include "adapt.h"
IMDBG_EXPORT_START()

#include "SDL.h"

struct gdb_raw_message {
	struct gdb_raw_message *next;
	int size;
	char data[0];
};

struct gdb_current_file {
	int lineno;
	int size;
	int uc;
	char data[0];
};

struct gdb_state {
	uint64_t msgid;
	uint64_t current_fileid;
	struct gdb_raw_message *msg, **last_msg;
	struct gdb_current_file *current_file;
};

struct gdb_breakpoint {
	int number;
	uintptr_t addr;
	struct gdb_current_file *file;
	int lineno;
	int times;
};

/** Initializes the connector at the start of the program. Passes the specified
 * `argc` and `argv` to GDB as arguments. */
void gdb_connector_open(int argc, char **argv);

/** Stops GDB and cleans up all connector resources at the end of the program. */
void gdb_connector_close(void);

/** Updates the specified `state` according to the received `event`. */
int gdb_connector_process_sdl_event(const SDL_Event *event, struct gdb_state *state);

/* Asynchronous GDB commands */
void gdb_request_current_file(void);
void gdb_request_breakpoint(const char *file, size_t file_len, unsigned int line);
void gdb_request_user_command(const char *command);
void gdb_request_async_mode(void);
void gdb_request_run(void);
void gdb_request_pause(void);
void gdb_request_continue(void);
void gdb_request_next(void);
void gdb_request_step(void);
void gdb_request_finish(void);

/** Initializes the specified state. */
void gdb_state_init(struct gdb_state *);

IMDBG_EXPORT_END()
