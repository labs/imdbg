cmake_minimum_required(VERSION 3.18)
project(imdbg)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 20)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(GLOBAL_OPTIONS
	-fno-strict-aliasing -fno-exceptions -ggdb3 -gdwarf-4)
set(IMDBG_OPTIONS -Wall)

if (ASAN)
	set(IMDBG_OPTIONS ${IMDBG_OPTIONS} -fsanitize=address,undefined)
endif ()

function(copy_file file)
	configure_file(
		${CMAKE_CURRENT_SOURCE_DIR}/${file}
		${CMAKE_CURRENT_BINARY_DIR}/${file}
		COPYONLY)
endfunction()

copy_file(fonts/UbuntuMono-R.ttf)

find_package(SDL2 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Threads REQUIRED)
add_library(imgui STATIC
	contrib/imgui/imgui.cpp
	contrib/imgui/imgui_demo.cpp
	contrib/imgui/imgui_draw.cpp
	contrib/imgui/imgui_tables.cpp
	contrib/imgui/imgui_widgets.cpp
	contrib/imgui/backends/imgui_impl_sdl2.cpp
	contrib/imgui/backends/imgui_impl_opengl2.cpp)
target_compile_options(imgui PRIVATE ${GLOBAL_OPTIONS})
target_link_options(imgui PRIVATE ${GLOBAL_OPTIONS})
target_link_libraries(imgui PRIVATE
	${SDL2_LIBRARIES} OpenGL::OpenGL)
target_include_directories(imgui PUBLIC
	contrib/imgui contrib/imgui/backends ${SDL2_INCLUDE_DIRS})

add_executable(imdbg
	imdbg.cpp
	gdb_connector.c
	utils.c)
target_link_libraries(imdbg PRIVATE
	imgui Threads::Threads)
target_compile_definitions(imdbg PUBLIC -D_GNU_SOURCE)
target_compile_options(imdbg PRIVATE ${GLOBAL_OPTIONS} ${IMDBG_OPTIONS})
target_link_options(imdbg PRIVATE ${GLOBAL_OPTIONS} ${IMDBG_OPTIONS})
