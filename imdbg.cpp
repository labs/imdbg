extern "C" {
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
}

#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_opengl2.h"

#include "SDL.h"
#include "SDL_opengl.h"

#include "gdb_connector.h"
#include "utils.h"

namespace imdbg {
struct loaded_file {
	char *filename;
	char *buf;
	size_t len;
	uint64_t fileid;
};

static struct {
	bool exit_requested;
	bool show_imgui_demo_window;
	bool gdb_command_sent;
	struct gdb_state gdb;
	uint64_t last_msgid;
	struct loaded_file loaded_file;
	char gdb_command_buffer[512];
	char gdb_last_command_buffer[512];
} state = {0};


static const char *next_line_end(const char *lbeg, const char *end)
{
	const char *cur = lbeg;
	while (cur < end) {
		switch (*cur) {
		case '\r':
			cur++;
			goto end;
		case '\n':
			cur++;
			[[fallthrough]];
		case '\0':
			goto end;
		}

		cur++;
	}

end:
	return cur;
}

static int load_file(struct loaded_file *out_lf, uint64_t fileid,
                     const char *filename)
{
	char path[PATH_MAX];
	int status = 0;
	size_t cap = 1024;
	size_t cur = 0;

	char *rp = realpath(filename, path);
	if (!rp)
		return errno;

	FILE *file = fopen(filename, "r");
	if (!file)
		return errno;

	char *buf = (char *)malloc(cap);
	if (!buf)
		die("Could not malloc file buffer\n");


	while (!feof(file)) {
		size_t free_space = cap - cur;
		if (free_space < 512) {
			cap *= 2;
			buf = (char *)realloc(buf, cap);
			if (!buf)
				die("Could not realloc file buffer\n");

			free_space = cap - cur;
		}
		size_t read = fread(&buf[cur], 1, free_space, file);
		if ((status = ferror(file)))
			break;
		cur += read;
	}

	fclose(file);

	if (status) {
		free(buf);
		return status;
	}

	*out_lf = (struct loaded_file){
		.filename = strdup(rp),
		.buf = buf,
		.len = cur,
		.fileid = fileid,
	};
	return status;
}

static void show_step_controls()
{
	if (ImGui::Button("Run"))
		gdb_request_run();
	ImGui::SameLine();
	if (ImGui::Button("Pause"))
		gdb_request_pause();
	ImGui::SameLine();
	if (ImGui::Button("Continue"))
		gdb_request_continue();
	ImGui::SameLine();
	if (ImGui::Button("Step over"))
		gdb_request_next();
	ImGui::SameLine();
	if (ImGui::Button("Step into"))
		gdb_request_step();
	ImGui::SameLine();
	if (ImGui::Button("Step out"))
		gdb_request_finish();
}

static void show_source_view(ImVec2 region)
{
	if (state.loaded_file.fileid != state.gdb.current_fileid) {
		int ret = load_file(&state.loaded_file, state.gdb.current_fileid,
				state.gdb.current_file->data);
		if (ret) {
			fprintf(stderr, "Could not load input file: %s (%d)\n",
					strerror(ret), ret);
		}
	}

	ImGui::BeginChild("SourceText", region, true,
			ImGuiWindowFlags_HorizontalScrollbar);
	static unsigned int broken_line = 0; // TODO replace with gdb state

	if (state.loaded_file.fileid && state.loaded_file.buf) {
		const char *line_beg = state.loaded_file.buf;
		unsigned int line = 1;
		auto list = ImGui::GetWindowDrawList();
		while (line_beg < &state.loaded_file.buf[state.loaded_file.len]) {
			const char *line_end = next_line_end(line_beg,
					&state.loaded_file.buf[state.loaded_file.len]);
			ImVec2 cp = ImGui::GetCursorScreenPos();
			bool hovered = ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows)
				&& ImGui::IsMouseHoveringRect(cp, {
						cp.x + ImGui::GetContentRegionAvail().x,
						cp.y + ImGui::GetTextLineHeightWithSpacing() });

			auto line_color = ImVec4(.7f, .7f, .7f, 1.f);
			if (hovered)
				line_color = ImVec4(1.f, 1.f, 1.f, 1.f);
			if (hovered && ImGui::IsMouseClicked(ImGuiMouseButton_Left)) {
				gdb_request_breakpoint(state.loaded_file.filename,
						strlen(state.loaded_file.filename), line);
				line_color = ImVec4(1.f, 0.f, 0.f, 1.f);
				broken_line = line;
			}

			ImGui::TextColored(line_color, "%3d", line);
			ImGui::SameLine();
			cp = ImGui::GetCursorScreenPos();
			if (line == broken_line) {
				list->AddRectFilled(cp,
						{ (float)INT32_MAX, cp.y + ImGui::GetTextLineHeightWithSpacing() },
						ImGui::GetColorU32({ 1.f, 0.f, 0.f, .3f }));
			}
			ImGui::TextUnformatted(line_beg, line_end);
			line_beg = line_end;
			line++;
		}
	}

	ImGui::EndChild();
}

static void show_gdb_console()
{
	ImGui::BeginChild("GdbOutput",
			{ 0, ImGui::GetContentRegionAvail().y - 2 * ImGui::GetTextLineHeightWithSpacing() },
			true, ImGuiWindowFlags_HorizontalScrollbar);
	for (struct gdb_raw_message *grm = state.gdb.msg; grm; grm = grm->next) {
		ImGui::TextUnformatted(grm->data, &grm->data[grm->size]);
		if (grm->data[grm->size - 1] != '\n')
			ImGui::SameLine(0, 0);
	}
	if (state.last_msgid != state.gdb.msgid) {
		state.last_msgid = state.gdb.msgid;
		ImGui::SetScrollHereY();
	}
	ImGui::EndChild();

	bool submit_command = false;
	if (state.gdb_command_sent) {
		ImGui::SetKeyboardFocusHere();
		state.gdb_command_sent = false;
	}
	submit_command |= ImGui::InputText("##GdbCommand",
			state.gdb_command_buffer,
			sizeof(state.gdb_command_buffer),
			ImGuiInputTextFlags_EnterReturnsTrue);
	ImGui::SameLine();
	submit_command |= ImGui::Button("Submit");

	if (submit_command) {
		if (state.gdb_command_buffer[0]) {
			gdb_request_user_command(state.gdb_command_buffer);
			memcpy(state.gdb_last_command_buffer,
					state.gdb_command_buffer,
					sizeof(state.gdb_command_buffer));
			state.gdb_command_buffer[0] = '\0';
		} else if (state.gdb_last_command_buffer[0]) {
			gdb_request_user_command(state.gdb_last_command_buffer);
		}
		state.gdb_command_sent = true;
	}
}

static void show_main_window(SDL_Window *window)
{
	int ww, wh;
	SDL_GetWindowSize(window, &ww, &wh);
	ImGui::SetNextWindowSize({ (float)ww, (float)wh });
	ImGui::SetNextWindowPos({ 0, 0 });
	constexpr ImGuiWindowFlags mwflags = 0
		| ImGuiWindowFlags_MenuBar
		| ImGuiWindowFlags_NoBringToFrontOnFocus
		| ImGuiWindowFlags_NoCollapse
		| ImGuiWindowFlags_NoMove
		| ImGuiWindowFlags_NoResize
		| ImGuiWindowFlags_NoTitleBar
		;
	if (ImGui::Begin("imdbg", nullptr, mwflags)) {
		if (ImGui::BeginMenuBar()) {
			if (ImGui::BeginMenu("Menu")) {
				if (ImGui::MenuItem("Quit"))
					state.exit_requested = true;

				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Help")) {
				ImGui::MenuItem("Show ImGui demo window", nullptr, &state.show_imgui_demo_window);
				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}

		show_step_controls();
		constexpr auto gdb_output_height = 300;
		if (ImGui::GetContentRegionAvail().y > gdb_output_height)
			show_source_view({ 0, ImGui::GetContentRegionAvail().y - gdb_output_height });
		show_gdb_console();

		ImGui::End();
	}
}
}

int main(int argc, char **argv)
{
	using namespace imdbg;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER))
		die("SDL_Init: %s\n", SDL_GetError());

#ifdef SDL_HINT_IME_SHOW_UI
	SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
#endif

	// Setup window
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
	SDL_Window* window = SDL_CreateWindow("imdbg", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
	SDL_GLContext gl_context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, gl_context);
	SDL_GL_SetSwapInterval(1); // Enable vsync

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= 0
		| ImGuiConfigFlags_NavEnableKeyboard
		;
	ImFontConfig fcfg;
	fcfg.OversampleH = fcfg.OversampleV = 3;
	io.Fonts->AddFontFromFileTTF("fonts/UbuntuMono-R.ttf", 17, &fcfg);

	ImGui::StyleColorsDark();

	// Backends
	ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
	ImGui_ImplOpenGL2_Init();

	// Setup GDB Connector
	gdb_connector_open(argc, argv);
	gdb_state_init(&state.gdb);

	constexpr ImVec4 clear_color(.2f, .2f, .2f, 1.0f);

	while (!state.exit_requested) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (gdb_connector_process_sdl_event(&event, &state.gdb))
				continue;

			ImGui_ImplSDL2_ProcessEvent(&event);
			if (event.type == SDL_QUIT)
				state.exit_requested = true;
			if (event.type == SDL_WINDOWEVENT
					&& event.window.event == SDL_WINDOWEVENT_CLOSE
					&& event.window.windowID == SDL_GetWindowID(window))
				state.exit_requested = true;
		}

		ImGui_ImplOpenGL2_NewFrame();
		ImGui_ImplSDL2_NewFrame();
		ImGui::NewFrame();

		show_main_window(window);

		if (state.show_imgui_demo_window)
			ImGui::ShowDemoWindow(&state.show_imgui_demo_window);

		ImGui::Render();
		glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
		glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
		SDL_GL_SwapWindow(window);
	}

	// Stop GDB Connector
	gdb_connector_close();

	// Cleanup
	ImGui_ImplOpenGL2_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(gl_context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	free(state.loaded_file.buf);
	free(state.loaded_file.filename);

	return EXIT_SUCCESS;
}
