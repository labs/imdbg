#pragma once

#include "adapt.h"
IMDBG_EXPORT_START()

#include <stdio.h>

/** Prints the specified error message into `stderr` and aborts the program. */
#define die(...) do { \
	fprintf(stderr __VA_OPT__(,) __VA_ARGS__); \
	abort(); \
} while (0)

/** Splits `data` into lines and prints each of the lines prepended with `prefix`. */
int fprint_prepended_lines(FILE *file, const char *prefix,
                           unsigned int data_len, const char *data);

IMDBG_EXPORT_END();
