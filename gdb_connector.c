#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "SDL.h"

#include "utils.h"

#include "gdb_connector.h"


/** The maximum number of outstanding tracked requests (those with a number
 * prepended, awaiting a callback handling) towards GDB. */
#define MAX_OUTSTANDING_REQUESTS 16

/** Callback type for parsing messages received from GDB. */
typedef int (*gdb_request_parse)(const char *buf, int sz, SDL_Event *sue);

static int gdb_register_request(gdb_request_parse parse);
static int gdb_parse_by_id(size_t id, char *buf, int sz, SDL_Event *sue);
static int gdb_parse_done(const char *buf, int sz, SDL_Event *sue);
static int gdb_parse_breakpoint(const char *buf, int sz, SDL_Event *sue);
static int gdb_parse_current_file(const char *buf, int sz, SDL_Event *sue);

enum gdb_response_type {
	GDB_RESPONSE_RAW,
	GDB_RESPONSE_ERROR,
	GDB_RESPONSE_CURRENT_FILE,
	GDB_RESPONSE_BREAKPOINT,
	GDB_RESPONSE__MAX,
};


enum gdb_connector_event_type {
	GDB_CONNECTOR_EVENT_NULL = 0,

	GDB_CONNECTOR_EVENT_CLOSE,
	GDB_CONNECTOR_EVENT_CURRENT_FILE,
	GDB_CONNECTOR_EVENT_BREAKPOINT,
	GDB_CONNECTOR_EVENT_USER_CMD,
	GDB_CONNECTOR_EVENT_ASYNC_MODE,

	GDB_CONNECTOR_EVENT_RUN,
	GDB_CONNECTOR_EVENT_PAUSE,
	GDB_CONNECTOR_EVENT_CONTINUE,
	GDB_CONNECTOR_EVENT_NEXT,
	GDB_CONNECTOR_EVENT_STEP,
	GDB_CONNECTOR_EVENT_FINISH,
};

struct gdb_connector_event {
	struct gdb_connector_event *next;
	enum gdb_connector_event_type type;
	int size;
	const char data[0];
};

struct gdb_connector_event_breakpoint {
	struct gdb_connector_event parent;
	unsigned int line;
	unsigned int file_len;
	char file[];
};

struct gdb_connector_event_user_cmd {
	struct gdb_connector_event parent;
	unsigned int len;
	char cmd[];
};


static struct {
	pthread_t tid;
	int gdb_pid;
	int ping[2];
	int wpipe[2];
	int rpipe[2];
	Uint32 event_type;
	bool want_write;
	pthread_mutex_t mutex;
	struct gdb_connector_event *queue, **eoq;
	gdb_request_parse outstanding_requests[MAX_OUTSTANDING_REQUESTS];
} gdb_connector;


enum gdb_result_value_type {
	GDB_RVAL_NULL       = 0,
	GDB_RVAL_CSTRING    = 1,
	GDB_RVAL_LIST_ITEM  = 2,
	GDB_RVAL_TUPLE_ITEM = 3,
};

struct gdb_result_value {
	enum gdb_result_value_type type;
	struct gdb_result_value *next; /* for list and tuple item: next item in list / tuple */
	struct gdb_result_value *val;  /* for list and tuple item: the actual value */
	char data[];                   /* for cstring: the actual data; for tuple: the list key */
};


/** Processes events internal to the connector. Return value of `false` signals
 * that the processing should stop (i.e. because a close event has been
 * processed). */
static bool gdb_connector_process_events(void)
{
	pthread_mutex_lock(&gdb_connector.mutex);
	struct gdb_connector_event *queue = gdb_connector.queue;
	gdb_connector.queue = NULL;
	gdb_connector.eoq = &gdb_connector.queue;
	pthread_mutex_unlock(&gdb_connector.mutex);

	char dummy[64];
	while (read(gdb_connector.ping[0], dummy, 64) > 0)
		;

	for (struct gdb_connector_event *q = queue; q; q = q->next) {
		char cmdbuf[128];
		int printed = 0;
		const char *pos, *end;

		switch (q->type) {
		case GDB_CONNECTOR_EVENT_NULL:
			die("Invalid event");

		case GDB_CONNECTOR_EVENT_CLOSE: {
			close(gdb_connector.rpipe[0]);
			close(gdb_connector.wpipe[1]);
			close(gdb_connector.ping[0]);
			close(gdb_connector.ping[1]);

			if (waitpid(gdb_connector.gdb_pid, NULL, 0) < 0)
				die("Failed to wait for GDB: %m");
			return false;
		}

		case GDB_CONNECTOR_EVENT_CURRENT_FILE: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-file-list-exec-source-file\n",
					gdb_register_request(gdb_parse_current_file));
			break;
		}

		case GDB_CONNECTOR_EVENT_BREAKPOINT: {
			struct gdb_connector_event_breakpoint *bp =
				(struct gdb_connector_event_breakpoint *)q;
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-break-insert %.*s:%d\n", gdb_register_request(gdb_parse_breakpoint),
					bp->file_len, bp->file, bp->line);
			break;
		}

		case GDB_CONNECTOR_EVENT_USER_CMD: {
			struct gdb_connector_event_user_cmd *uc =
				(struct gdb_connector_event_user_cmd *)q;
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%.*s\n", uc->len, uc->cmd);
			break;
		}

		case GDB_CONNECTOR_EVENT_ASYNC_MODE: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-gdb-set non-stop 1\n%d-gdb-set mi-async 1\n",
					gdb_register_request(gdb_parse_done),
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_RUN: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-run\n",
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_PAUSE: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-interrupt --all\n",
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_CONTINUE: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-continue --all\n",
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_NEXT: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-next\n",
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_STEP: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-step\n",
					gdb_register_request(gdb_parse_done));
			break;
		}

		case GDB_CONNECTOR_EVENT_FINISH: {
			printed = snprintf(cmdbuf, sizeof(cmdbuf),
					"%d-exec-finish\n",
					gdb_register_request(gdb_parse_done));
			break;
		}
		}

		if (printed) {
			pos = cmdbuf;
			end = cmdbuf + printed;

			while (end > pos)
			{
				ssize_t sz = write(gdb_connector.wpipe[1], pos, end - pos);
				if (sz < 0)
					die("Failed to write to GDB: %m");
				pos += sz;
			}
			fprint_prepended_lines(stdout, "[GDB<-]  ",
					(unsigned int)strlen(cmdbuf), cmdbuf);
		}
	}

	struct gdb_connector_event *qnext;
	for (struct gdb_connector_event *q = queue; q; q = qnext) {
		qnext = q->next;
		free(q);
	}

	return true;
}

/** Processes messages received from GDB. */
static void gdb_connector_process_messages(void)
{
#define BFSZ 4096
	char buf[BFSZ];
	ssize_t sz = read(gdb_connector.rpipe[0], buf, BFSZ-1);
	if (sz < 0)
		die("Failed to read: %m");

	buf[sz] = 0;

	fprint_prepended_lines(stdout, "[GDB->]  ", (unsigned int)sz, buf);

	size_t id = 0;
	int pos;
	for (pos = 0; (buf[pos] >= '0' && buf[pos] <= '9'); pos++)
		id = id * 10 + (buf[pos] - '0');

	SDL_Event sue;
	if (!id || !gdb_parse_by_id(id, buf + pos, sz - pos, &sue))
	{
		struct gdb_raw_message *grm = calloc(1, sz + 1 + sizeof *grm);
		memcpy(grm->data, buf, sz+1);
		grm->size = sz;

		sue.type = gdb_connector.event_type + GDB_RESPONSE_RAW;
		sue.user.data1 = grm;

		SDL_PushEvent(&sue);
	}
#undef BFSZ
}

static void *gdb_connector_main(void *_)
{
	while (1) {
		struct pollfd pfd[3] = {
			{
				.fd = gdb_connector.rpipe[0],
				.events = POLLIN,
			},
			{
				.fd = gdb_connector.wpipe[1],
				.events = gdb_connector.want_write
					? (short) POLLOUT
					: (short) 0,
			},
			{
				.fd = gdb_connector.ping[0],
				.events = POLLIN,
			},
		};

		int pout = poll(pfd, 3, 1000);

		if (pout < 0)
			die("Failed to poll: %m");

		if (pfd[0].revents & (POLLERR | POLLHUP)) {
			/* GDB has closed - push a quit event to the frontend */
			SDL_Event cle = { .type = SDL_QUIT };
			SDL_PushEvent(&cle);
			return NULL;
		}

		if (pfd[2].revents & POLLIN) {
			if (!gdb_connector_process_events())
				return NULL;
		}

		if (pfd[0].revents & POLLIN)
			gdb_connector_process_messages();
	}
}

void gdb_connector_open(int argc, char **argv)
{
	gdb_connector.event_type = SDL_RegisterEvents(GDB_RESPONSE__MAX);

	if (pipe2(gdb_connector.ping, O_NONBLOCK) < 0)
		die("Couldn't initialize GDB ping pipe: %m");

	if (pipe(gdb_connector.rpipe) < 0)
		die("Couldn't initialize GDB read pipe: %m");

	if (pipe(gdb_connector.wpipe) < 0)
		die("Couldn't initialize GDB write pipe: %m");

	int pid = fork();
	if (pid < 0)
		die("Couldn't fork: %m");

	if (!pid)
	{
		char **nargv = (char **) malloc((argc+2) * sizeof (char *));
		nargv[0] = strdup("gdb");
		nargv[1] = strdup("--interpreter=mi3");
		memcpy(nargv + 2, argv + 1, argc * sizeof (char *));

		close(gdb_connector.wpipe[1]);
		close(gdb_connector.rpipe[0]);

		if (dup2(gdb_connector.wpipe[0], 0) < 0)
			die("Couldn't dup2 wpipe: %m");

		if (dup2(gdb_connector.rpipe[1], 1) < 0)
			die("Couldn't dup2 rpipe: %m");

		close(gdb_connector.wpipe[0]);
		close(gdb_connector.rpipe[1]);

		if (execvp("gdb", nargv) < 0)
			die("Couldn't exec: %m");
	}

	close(gdb_connector.wpipe[0]);
	close(gdb_connector.rpipe[1]);

	gdb_connector.gdb_pid = pid;

	if (pthread_mutex_init(&gdb_connector.mutex, NULL) < 0)
		die("Couldn't initialize mutex: %m");

	gdb_connector.queue = NULL;
	gdb_connector.eoq = &gdb_connector.queue;
	gdb_request_async_mode();

	pthread_create(&gdb_connector.tid, NULL, gdb_connector_main, NULL);
}

static void gdb_connector_push_event(struct gdb_connector_event *event)
{
	pthread_mutex_lock(&gdb_connector.mutex);
	*gdb_connector.eoq = event;
	gdb_connector.eoq = &event->next;
	pthread_mutex_unlock(&gdb_connector.mutex);

	char x = 0;
	write(gdb_connector.ping[1], &x, 1);
}

void gdb_connector_close(void)
{
	struct gdb_connector_event close_event = {
		.type = GDB_CONNECTOR_EVENT_CLOSE,
	};

	gdb_connector_push_event(&close_event);

	int e = pthread_join(gdb_connector.tid, NULL);
	if (e < 0)
		die("Couldn't join pthread: %s", strerror(e));
}

/** Pushes a simple (i.e. ID with no additional data) event to the connector. */
static void gdb_request_simple(enum gdb_connector_event_type type)
{
	struct gdb_connector_event *e = (struct gdb_connector_event *) malloc(sizeof *e);
	e->next = NULL;
	e->type = type;

	gdb_connector_push_event(e);
}

void gdb_request_current_file(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_CURRENT_FILE);
}

void gdb_request_breakpoint(const char *file, size_t file_len, unsigned int line)
{
	int size = sizeof(struct gdb_connector_event_breakpoint) + file_len;
	struct gdb_connector_event_breakpoint *e = malloc(size);
	e->parent.next = NULL;
	e->parent.type = GDB_CONNECTOR_EVENT_BREAKPOINT;
	e->parent.size = size;
	e->line = line;
	e->file_len = file_len;
	memcpy(e->file, file, file_len);

	gdb_connector_push_event((struct gdb_connector_event *)e);
}

void gdb_request_async_mode(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_ASYNC_MODE);
}

void gdb_request_user_command(const char *command)
{
	if (command[0] == '-' || isdigit(command[0])) {
		printf("User attempted to run MI command\n");
		return;
	}

	size_t cmd_len = strlen(command);
	int size = sizeof(struct gdb_connector_event_user_cmd) + cmd_len;
	struct gdb_connector_event_user_cmd *e = malloc(size);
	e->parent.next = NULL;
	e->parent.type = GDB_CONNECTOR_EVENT_USER_CMD;
	e->parent.size = size;
	e->len = cmd_len;
	memcpy(e->cmd, command, cmd_len);

	gdb_connector_push_event((struct gdb_connector_event *)e);
}

void gdb_request_run(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_RUN);
}

void gdb_request_pause(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_PAUSE);
}

void gdb_request_continue(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_CONTINUE);
}

void gdb_request_next(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_NEXT);
}

void gdb_request_step(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_STEP);
}

void gdb_request_finish(void)
{
	gdb_request_simple(GDB_CONNECTOR_EVENT_FINISH);
}

static int gdb_register_request(gdb_request_parse parse)
{
	for (size_t i = 0; i < MAX_OUTSTANDING_REQUESTS; i++)
		if (!gdb_connector.outstanding_requests[i])
		{
			gdb_connector.outstanding_requests[i] = parse;
			return i + 1;
		}

	die("Too many outstanding_requests");
	return 0xdead;
}

static int gdb_parse_by_id(size_t id, char *buf, int sz, SDL_Event *sue)
{
	gdb_request_parse parse = NULL;
	id--;
	if ((id < 0) || (id >= MAX_OUTSTANDING_REQUESTS) || !(parse = gdb_connector.outstanding_requests[id]))
		return 0;

	gdb_connector.outstanding_requests[id] = NULL;
	return parse(buf, sz, sue);
}

#define BAD(...) do { \
	printf(__VA_ARGS__); \
	if (val) free(val); \
	return NULL; \
} while (0)

struct gdb_result_value *gdb_parse_result_cstring(const char **buf, int *sz)
{
	struct gdb_result_value *val = NULL;
	const char *e;
	for (e = *buf; (e < *buf + *sz); e++)
	{
		switch (*e) {
		case '"': break;
		case '\\': if (e+1 >= *buf + *sz) BAD("Incomplete string: %s\n", *buf);
				   e++;
		default: continue;
		}

		int consume = (e - *buf);
		val = calloc(1, consume + 1 + sizeof *val);
		memcpy(val->data, *buf, consume);

		*sz -= consume;
		*buf += consume + 1;

		val->data[consume] = 0;
		val->type = GDB_RVAL_CSTRING;
		return val;
	}

	BAD("Runaway string: %s\n", *buf);
}

struct gdb_result_value *gdb_parse_result_tuple(const char **buf, int *sz,
		bool want_key)
{
	struct gdb_result_value *val = NULL;
	const char *e;

	if (!*sz)
		BAD("Runaway tuple: %s\n", *buf);

	if (want_key)
	{
		if ((*buf)[0] == '}')
		{
			(*buf)++;
			(*sz)--;
			val = calloc(1, 1 + sizeof *val);
			val->data[0] = 0;
			val->type = GDB_RVAL_TUPLE_ITEM;
			val->val = NULL;
			val->next = NULL;
			return val;
		}

		/* Find = */
		for (e = *buf; (e < *buf + *sz) && (*e != '='); e++)
			;

		if (e == *buf + *sz)
			BAD("End of buffer while looking for =: %s\n", *buf);

		/* Store item name */
		e++;
		int consume = (e - *buf);
		if (*sz < consume + 1)
			BAD("End of buffer while looking for value: %s\n", *buf);

		val = calloc(1, 1 + consume + sizeof *val);
		memcpy(val->data, *buf, consume-1);
		val->data[consume] = 0;
		val->type = GDB_RVAL_TUPLE_ITEM;

		*sz -= consume;
		*buf += consume;
	}
	else
	{
		val = calloc(1, sizeof *val);
		val->type = GDB_RVAL_LIST_ITEM;

		if ((*buf)[0] == ']')
		{
			(*buf)++;
			(*sz)--;
			val->val = NULL;
			val->next = NULL;
			return val;
		}
	}

	const char c = (*buf)[0];
	(*sz)--;
	(*buf)++;

	switch (c) {
	case '"':
		val->val = gdb_parse_result_cstring(buf, sz);
		break;
	case '[':
		val->val = gdb_parse_result_tuple(buf, sz, 0);
		break;
	case '{':
		val->val = gdb_parse_result_tuple(buf, sz, 1);
		break;
	default:
		BAD("Can't infer data type from %c (%d): %s\n", c, c, *buf);
		break;
	}

	if (!val->val)
	{
		free(val);
		return NULL;
	}

	if (!*sz)
	{
		val->next = NULL;
		return val;
	}

	const char nc = (*buf)[0];
	(*buf)++;
	(*sz)--;

	switch (nc) {
	case ',':
		if ((val->next = gdb_parse_result_tuple(buf, sz, want_key)))
			return val;

		free(val);
		return NULL;
	case '}':
	case ']':
		val->next = NULL;
		return val;
	}

	if (*sz && ((*buf)[0] == ',')) {
		(*buf)++;
		(*sz)--;
	}

	return val;
}

void gdb_dump_result_tuple(struct gdb_result_value *val, int indent)
{
	const char indent_string[] = "                                ";
#define INDENT (&indent_string[sizeof(indent_string) - 1 - indent])
	char delim;

	switch (val->type)
	{
	case GDB_RVAL_CSTRING:
		printf("\"%s\"", val->data);
		break;
	case GDB_RVAL_TUPLE_ITEM:
		delim = '{';
		indent += 2;
		for (struct gdb_result_value *v = val; v && v->val; v = v->next)
		{
			printf("%c\n%s%s=", delim, INDENT, v->data);
			gdb_dump_result_tuple(v->val, indent);
			delim = ',';
		}
		indent -= 2;
		if (delim == '{')
			printf("{}");
		else
			printf("\n%s}", INDENT);
		break;
	case GDB_RVAL_LIST_ITEM:
		delim = '[';
		indent += 2;
		for (struct gdb_result_value *v = val; v && v->val; v = v->next)
		{
			printf("%c\n%s", delim, INDENT);
			gdb_dump_result_tuple(v->val, indent);
			delim = ',';
		}
		indent -= 2;
		if (delim == '[')
			printf("[]");
		else
			printf("\n%s]", INDENT);
		break;
	default:
		die("Be or not to be? That is the question.");
	}
}


#define BAD_ROOT(...) do { \
	printf(__VA_ARGS__); \
	gdb_result_val_free(root); \
	return EINVAL; \
} while (0)

#define BAD_SIMPLE(...) do { \
	printf(__VA_ARGS__); \
	return EINVAL; \
} while (0)

#define CONSUME(n, BAD_X) do { \
	buf += n; \
	if (buf > end) \
	BAD_X("Truncated message\n"); \
} while (0)

#define CMP(str, BAD_X) do { \
	const char *b = buf; \
	CONSUME(sizeof(str)-1, BAD_X); \
	if (strncmp(b, str, sizeof(str)-1)) \
	BAD_X("Malformed message at %s:%d since: %s\n", __FILE__, __LINE__, b); \
} while (0)

static void gdb_result_val_free(struct gdb_result_value *val)
{
	while (val) {
		gdb_result_val_free(val->val);
		struct gdb_result_value *val_next = val->next;
		free(val);
		val = val_next;
	}
}

static struct gdb_result_value * gdb_result_get_error(const char **buf, int *sz)
{
	const int s = sizeof("^error,") - 1;
	if (*sz < s)
		die("Error in error handling.");

	if (strncmp(*buf, "^error,", s))
		return NULL;

	*buf += s;
	*sz -= s;
	return gdb_parse_result_tuple(buf, sz, true);
}

static bool gdb_result_string_to_num(const struct gdb_result_value *val, uintmax_t *uout, intmax_t *sout)
{
	if (!uout == !sout)
		return false;

	if (val->type != GDB_RVAL_CSTRING)
		return false;

	int ret = uout ? sscanf(val->data, "%ju", uout) : sscanf(val->data, "%jd", sout);
	if (!ret || (ret == EOF))
		return false;
	else
		return true;
}

static bool gdb_result_hex_to_num(const struct gdb_result_value *val, uintmax_t *uout)
{
	if (!uout)
		return false;

	if (val->type != GDB_RVAL_CSTRING)
		return false;

	int ret = sscanf(val->data, "0x%jx", uout);
	return ret && (ret != EOF);
}

static int gdb_parse_current_file(const char *buf, int sz, SDL_Event *sue)
{
	const char *end = buf + sz;
	CMP("^done,", BAD_SIMPLE);

	intmax_t lineno = 0;
	char *filename = NULL;
	struct gdb_result_value *root = gdb_parse_result_tuple(&buf, &sz, true);

	for (struct gdb_result_value *val = root; val; val = val->next) {
		if (val->type != GDB_RVAL_TUPLE_ITEM)
			BAD_ROOT("Expected to parse a list item\n");

		if (strcmp("line", val->data) == 0) {
			if (!gdb_result_string_to_num(val->val, NULL, &lineno))
				BAD_ROOT("line could not be parsed as an integer\n");

		} else if (strcmp("file", val->data) == 0) {
			struct gdb_result_value *file_val = val->val;
			if (file_val->type != GDB_RVAL_CSTRING)
				BAD_ROOT("file must be a string\n");
			filename = file_val->data;

		} else if (
				(strcmp("fullname", val->data) == 0)
				||  (strcmp("macro-info", val->data) == 0)
				||  (strcmp("frame", val->data) == 0))
		{
			/* ignore */
		} else {
			printf("Unknown key '%s'\n", val->data);
		}
	}

	if (!filename)
		BAD_ROOT("key 'file' not provided - strange...\n");

	size_t fnlen = strlen(filename);
	struct gdb_current_file *gcf = calloc(1, fnlen + 1 + sizeof *gcf);
	gcf->lineno = lineno;
	gcf->size = fnlen;
	memcpy(gcf->data, filename, fnlen);
	gcf->data[fnlen] = '\0';

	sue->type = gdb_connector.event_type + GDB_RESPONSE_CURRENT_FILE;
	sue->user.data1 = gcf;
	SDL_PushEvent(sue);

	gdb_result_val_free(root);
	return 1;
}

static int gdb_parse_done(const char *buf, int sz, SDL_Event *sue)
{
	const char *end = buf + sz;
	CMP("^done", BAD_SIMPLE);
	return 1;
}

static int gdb_parse_breakpoint(const char *buf, int sz, SDL_Event *sue)
{
	const char *end = buf + sz;

	struct gdb_result_value *gerr = gdb_result_get_error(&buf, &sz);
	if (gerr)
	{
		sue->type = gdb_connector.event_type + GDB_RESPONSE_ERROR;
		sue->user.data1 = gerr;
		SDL_PushEvent(sue);
		return 1;
	}

	CMP("^done,", BAD_SIMPLE);

	struct gdb_result_value *root = gdb_parse_result_tuple(&buf, &sz, true);

	if (strcmp("bkpt", root->data))
		BAD_ROOT("breakpoint request must return bkpt, got %s\n", root->data);

	intmax_t snum;
	uintmax_t unum;

	struct gdb_breakpoint *bkpt = calloc(1, sizeof *bkpt);

	for (struct gdb_result_value *val = root->val; val; val = val->next) {
		if (!strcmp("number", val->data)) {
			if (gdb_result_string_to_num(val->val, NULL, &snum))
				bkpt->number = snum;
			else
				BAD_ROOT("Breakpoint number must be a number\n");
		}
		else if (!strcmp("line", val->data)) {
			if (gdb_result_string_to_num(val->val, NULL, &snum))
				bkpt->lineno = snum;
			else
				BAD_ROOT("Line number must be a number\n");
		}
		else if (!strcmp("times", val->data)) {
			if (gdb_result_string_to_num(val->val, NULL, &snum))
				bkpt->lineno = snum;
			else
				BAD_ROOT("Line number must be a number\n");
		}
		else if (!strcmp("addr", val->data)) {
			if (gdb_result_hex_to_num(val->val, &unum))
				bkpt->addr = unum;
			else
				BAD_ROOT("Address must be a hex num\n");
		}

	}

	sue->type = gdb_connector.event_type + GDB_RESPONSE_BREAKPOINT;
	sue->user.data1 = bkpt;
	SDL_PushEvent(sue);

	gdb_result_val_free(root);
	return 1;
}

void gdb_state_init(struct gdb_state *s)
{
	s->msg = NULL;
	s->last_msg = &s->msg;
}

int gdb_connector_process_sdl_event(const SDL_Event *event, struct gdb_state *state)
{
	switch ((enum gdb_response_type)(event->type - gdb_connector.event_type)) {
	case GDB_RESPONSE_RAW: {
		struct gdb_raw_message *grm = (struct gdb_raw_message *) event->user.data1;

		if ((grm->size >= (int) sizeof("(gdb)")) && !strncmp(grm->data, "(gdb)", sizeof("(gdb)")-1))
			gdb_request_current_file();

		state->msgid++;
		*state->last_msg = grm;
		state->last_msg = &grm->next;
		return 1;
	}

	case GDB_RESPONSE_ERROR: {
		struct gdb_result_value *val = (struct gdb_result_value *) event->user.data1;
		if ((val->type != GDB_RVAL_TUPLE_ITEM) || strcmp("msg", val->data))
		{
			printf("Error in parsing the error response message\n");
			return 0;
		}

		printf("ERROR ERROR ERROR ERROR\n");
		printf("The error message is: %s\n", val->val->data);
		printf("FIX THE ERROR YOU MORON\n");
		return 1;
	}

	case GDB_RESPONSE_CURRENT_FILE: {
		if (state->current_file)
			free(state->current_file);
		do {
			state->current_fileid++;
		} while (state->current_fileid == 0);
		state->current_file = (struct gdb_current_file *) event->user.data1;
		return 1;
	}

	case GDB_RESPONSE_BREAKPOINT: {
		printf("Got a new breakpoint!\n");
		printf("Please, do something!\n");
		printf("Not implemented yet …\n");
		free(event->user.data1);
		return 1;
	}

	case GDB_RESPONSE__MAX:
		die("Invalid response type");
	}

	return 0;
}
