#include "utils.h"

int fprint_prepended_lines(FILE *file, const char *const prefix,
                           unsigned int data_len, const char *const data)
{
	int printed = 0;
	const char *line_start = data;
	const char *end = &data[data_len];

	for (const char *pos = data; pos < end; pos++) {
		if (pos >= end || *pos == '\n') {
			printed += fprintf(file, "%s%.*s\n", prefix,
					(int)(pos - line_start), line_start);
			line_start = ++pos;
		}
	}

	return printed;
}
