#pragma once
/**@file Generic tools for headers that adapt C++ APIs to C and vice-versa. */

#ifdef __cplusplus
#define IMDBG_EXPORT extern "C"
#define IMDBG_EXPORT_START() extern "C" {
#define IMDBG_EXPORT_END() }
#define IMDBG_RESTRICT
#else
#define IMDBG_EXPORT
#define IMDBG_EXPORT_START()
#define IMDBG_EXPORT_END()
#define IMDBG_RESTRICT restrict
#endif
